import React, { useContext } from "react";
import CommonContext from "../../CommonContext";
import { Navbar } from "../../components/Navbar";

export const About = () => {
  const { variable } = useContext(CommonContext)
  return (
    <div> <Navbar />{variable.title}</div>
  )
}