import React, { useContext, useState } from "react";
import AuthContext from "../../AuthContext";

export const Login = () => {

    const { login} = useContext(AuthContext);
    const [name, setName] = useState();

  return (
    <>
      <h1>Please, log in!</h1>

      <label>Name:</label>
      <input
        type="text"
        onChange={(event) => {
          setName(event.target.value);
        }}
      />
      <button onClick={() => login(name)}>Log in</button>
    </>
  );
}
