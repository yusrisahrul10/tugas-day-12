import React from "react";

const AuthContext = React.createContext({ name: '', auth: false });

export default AuthContext;