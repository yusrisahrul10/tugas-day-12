import React, { useState } from "react";
import AuthContext from "../AuthContext";

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState({ name: '', auth: true });

  const login = (name) => {
    setUser((user) => ({
      name: name,
      auth: true,
    }));
  };

  const logout = () => {
    setUser((user) => ({
      name: '',
      auth: false,
    }));
  };

  return (
    <AuthContext.Provider value={{ user, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};
