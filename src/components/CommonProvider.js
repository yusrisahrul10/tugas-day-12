import React, { useState } from 'react'
import CommonContext from '../CommonContext'


export const CommonProvider = ({ children }) => {
    const [variable, setVariable] = useState({
        title: "home",
        color: "#FF0000"
    })

    const changeVariable = (newTitle, newColor) => {
        setVariable({title: newTitle, color: newColor})
    }
  return (
    <CommonContext.Provider value={{variable, changeVariable}}>
        {children}
    </CommonContext.Provider>
  )
}
