import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  Button,
} from "@mui/material";
import React, { useState, useContext } from "react";
import { Menu as MenuIcon } from "@mui/icons-material";
import { CameraAlt } from "@mui/icons-material";
import { NavLink } from "react-router-dom/cjs/react-router-dom";
import "../../css/Navbar.css";
import CommonContext from "../../CommonContext";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import AuthContext from "../../AuthContext";

export const Navbar = () => {
  const [anchorEl, setAnchorEl] = useState(null);

  const { variable, changeVariable } = useContext(CommonContext);

  const { logout } = useContext(AuthContext)
  
  const navbarTheme = createTheme({
    palette: {
      mode: "dark",
      primary: {
        main: variable.color,
      },
    },
  });

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <ThemeProvider theme={navbarTheme}>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static" enableColorOnDark color="primary">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              sx={{ mr: 2 }}
            >
              <CameraAlt />
            </IconButton>
            <Typography variant="h6" component="div">
              <NavLink
                exact
                to="/"
                activeClassName="active-link header-nav-link"
                className="header-nav-link"
                onClick={() => {
                  changeVariable("Home", "#FF0000");
                }}
              >
                My App
              </NavLink>
            </Typography>
            <Typography variant="h6" component="div" sx={{ ml: 2 }}>
              <NavLink
                to="/about"
                activeClassName="active-link header-nav-link"
                className="header-nav-link"
                onClick={() => {
                  changeVariable("About", "#ffffff");
                }}
              >
                About
              </NavLink>
            </Typography>
            <Typography
              variant="h6"
              component="div"
              sx={{ flexGrow: 1, ml: 2 }}
            >
              <NavLink
                to="/memory"
                activeClassName="active-link header-nav-link"
                className="header-nav-link"
                onClick={() => {
                  changeVariable("Memory", "#1976d2");
                }}
              >
                Memory
              </NavLink>
            </Typography>
            <IconButton
              size="large"
              edge="end"
              color="inherit"
              aria-label="menu"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              sx={{ ml: 2, display: { xl: "none", xs: "block" } }}
              onClick={handleMenuOpen}
            >
              <MenuIcon />
            </IconButton>
            <Button
              size="large"
              edge="end"
              color="inherit"
              aria-label="menu"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              sx={{ ml: 2, display: { xl: "block", xs: "none" } }}
              onClick={() => {
                logout()
              }}
            >
              Logout
            </Button>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorEl)}
              onClose={handleMenuClose}
            >
              <MenuItem onClick={handleMenuClose}>
                <NavLink
                  exact
                  to="/"
                  activeClassName="active-link nav-link"
                  className="nav-link"
                >
                  My App
                </NavLink>
              </MenuItem>
              <MenuItem onClick={handleMenuClose}>
                <NavLink
                  to="/about"
                  activeClassName="active-link nav-link"
                  className="nav-link"
                >
                  About
                </NavLink>
              </MenuItem>
              <MenuItem onClick={handleMenuClose}>
                <NavLink
                  to="/memory"
                  activeClassName="active-link nav-link"
                  className="nav-link"
                >
                  Memory
                </NavLink>
              </MenuItem>
            </Menu>
          </Toolbar>
        </AppBar>
      </Box>
    </ThemeProvider>
  );
};
