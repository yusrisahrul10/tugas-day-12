import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { UserProvider } from "./components/UserProvider";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { About } from "./page/About";
import { Memory } from "./page/Memory";
import { CommonProvider } from "./components/CommonProvider";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Router>
      <UserProvider>
        <CommonProvider>
          <Switch>
            <Route exact path="/">
              <App />
            </Route>
            <Route exact path="/about">
              <About />
            </Route>
            <Route exact path="/memory">
              <Memory />
            </Route>
          </Switch>
        </CommonProvider>
      </UserProvider>
    </Router>
  </React.StrictMode>
);
