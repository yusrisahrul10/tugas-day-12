import React, { useContext } from "react";
import CommonContext from "./CommonContext";
import { Navbar } from "./components/Navbar";
import { Login } from "./page/Login";
import AuthContext from "./AuthContext";

export default function App() {

  const { variable } = useContext(CommonContext)

  const { user } = useContext(AuthContext)
  
  return user.auth ? (
    <div><Navbar /> <h1>Hello, {user.name}! </h1> {variable.title}</div>
  ) : <Login />;
}
